#visualizador molecular

-Es  un programa que permite la visualización e información de una mòlecula que se encuentra en formato "mol2".

#Obtención del programa

-El programa se obtiene clonando este repositorio, luego activar el enterno virtual e ingresar al repositorio contenedor del visualizador para luego escribir el comando "python visualizador.py" y luego ejecutar el programa.

#prerrequisitos

-Python 3. Desarrollado y probado en 3.5.6
-Diseñador de interfaces Glade.
-Conjunto de bibliotecas multiplataforma para desarrollar interfaces gráficas GTK.

#Objetivo

-Lograr la visualización de diferentes móleculas de una forma rápida e interactiva.

#Controles

-Las principales herramientas son el mouse y el teclado para seleccionar la mólecula que se quiere visualizar.

#sobre el código

-El visualizador es un programa que permite la visualización de diferentes móleculas, esto se logra haciendo que en primer lugar el código sea capaz de seleccionar la carpeta en donde se encuentran los archivos (con ayuda de la interfaz gráfica creada en Glade) y luego se val filtrando los archivos según su terminación (si estos terminan en .mol2 aparecen en el listado que se muestra en el programa), con el botón que permite seleccionar la carpeta se obtiene la ruta y se le muestra al usuario en que carpeta este se encuentra.
-Para la información se reúnen datos sacados del archivo y luego se va escribiendo segun la columna en el que se encuentre, para encontrar la cantidad de átomos y cuánto porcentaje hay de cada uno de ellos se hace mediante un for que recorre el arreglo contenedor de datos con los átomos que existen.

#Errores en el programa
-No es capaz de guardar los cambios luego de editar.

#Autor

-Rachell Scarlett Aravena Martínez.