import gi
from os import listdir, system

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class visualizar():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("menu_principal.glade")
        self.visualizador = self.builder.get_object("visualizar")
        self.visualizador.set_default_size(600, 400)
        self.visualizador.connect("destroy", Gtk.main_quit)
        self.visualizador.show_all()
        self.b_carpeta = self.builder.get_object("abrir_carpeta")
        self.b_carpeta.connect("file-set", self.obtener_carpeta)
        self.info = self.builder.get_object("info")
        self.info_mol = self.builder.get_object("info_mol")
        self.imagen = self.builder.get_object("imagen")
        self.b_visualiza = self.builder.get_object("visualiza")
        self.b_visualiza.connect("clicked", self.abrir_editor)

        # valores por defecto
        self.carpeta = ""
        self.archivos = []

        self.armar_tabla()

        self.contenido.connect("cursor-changed", self.archivo_seleccionado)

    def obtener_carpeta(self, kjfdk=None):
        self.carpeta = self.b_carpeta.get_filename()
        self.info.set_text("Carpeta: " + self.carpeta)
        todos_archivos = listdir(self.carpeta)
        archivos = []

        # aqui filtrar archivos que no sean .mol o .mol2
        for nombre in todos_archivos:
            # saltarse los que no tienen extension
            if "." not in nombre:
                continue

            formato = nombre.split(".")[-1]
            if formato == "mol2":
                archivos.append(nombre)

        self.remove_all_data()

        # rellenar tabla
        for archivo in archivos:
            self.modelo.append([archivo])

    # Método para remover todos los datos
    def remove_all_data(self):
        # Verificamos su aún hay datos en el modelo
        if len(self.modelo) != 0:
            # remove all the entries in the model
            # Removemos todas en entradas (datos) en el modelo
            for i in range(len(self.modelo)):
                iter = self.modelo.get_iter(0)
                self.modelo.remove(iter)

    def archivo_seleccionado(self, btn=None):
        # ejemplo3-1.py
        model, it = self.contenido.get_selection().get_selected()

        if model is None or it is None:
            return

        seleccionado = model.get_value(it, 0)
        self.mostrar_info(seleccionado)
        self.mostrar_imagen(seleccionado)

    def mostrar_imagen(self, seleccionado):
        system("pymol " + self.carpeta + "/" + seleccionado + " -x -g molecula.png -c -Q")
        self.imagen.set_from_file("molecula.png")

    def abrir_editor(self, seleccionado):
        model, it = self.contenido.get_selection().get_selected()

        if model is None or it is None:
            return

        seleccionado = model.get_value(it, 0)
        editor(seleccionado, self.carpeta)

    def mostrar_info(self, seleccionado):
        informacion = "nombre: " + seleccionado + "\n\n"

        atomos = []
        with open(self.carpeta + "/" + seleccionado) as archivo:
            seccion_atomos = False

            for linea in archivo.read().split("\n"):
                if seccion_atomos is True:
                    atomo = linea.split()

                    if len(atomo) > 5:
                        atomos.append(atomo[5])

                if linea == "@<TRIPOS>ATOM":
                    seccion_atomos = True
                elif linea == "@<TRIPOS>BOND":
                    seccion_atomos = False

        total = 0
        n_atomos = {}
        n_atomos["Carbono"] = 0
        n_atomos["Hidrogeno"] = 0
        n_atomos["Nitrogeno"] = 0
        n_atomos["Oxigeno"] = 0
        n_atomos["Otro"] = 0

        for el in atomos:
            if "." in el:
                el = el.split(".")[0]

            total = total + 1

            if el == "C":
                n_atomos["Carbono"] = n_atomos["Carbono"] + 1
            elif el == "H":
                n_atomos["Hidrogeno"] = n_atomos["Hidrogeno"] + 1
            elif el == "N":
                n_atomos["Nitrogeno"] = n_atomos["Nitrogeno"] + 1
            elif el == "O":
                n_atomos["Oxigeno"] = n_atomos["Oxigeno"] + 1
            else:
                n_atomos["Otro"] = n_atomos["Otro"] + 1

        for elemento, contador in n_atomos.items():
            porcentaje = contador / total * 100
            informacion = informacion + elemento + ": " + str(int(porcentaje)) + "%\n"

        self.info_mol.set_text(informacion)
        return

    def armar_tabla(self):
        self.contenido = self.builder.get_object("contenido")
        self.modelo = Gtk.ListStore(str)
        self.contenido.set_model(model=self.modelo)

        celda = Gtk.CellRendererText()
        col = Gtk.TreeViewColumn("Nombre", celda, text=0)
        self.contenido.append_column(col)


class editor ():
    def __init__(self, archivo, carpeta):
        self.archivo = archivo
        self.carpeta = carpeta

        self.builder = Gtk.Builder()
        self.builder.add_from_file("ventana_guarda.glade")
        self.ventana = self.builder.get_object("editor")
        self.ventana.set_default_size(600, 400)
        self.ventana.show_all()
        self.nombre_mol = self.builder.get_object("nombre_molecula")
        self.info_mol = self.builder.get_object("info_molecula")
        self.b_guardar = self.builder.get_object("guardar")
        self.b_cancelar = self.builder.get_object("cancelar")
        with open(self.carpeta + "/" + self.archivo, "r") as molecula:
            datos = molecula.read()
            self.info_mol.get_buffer().set_text(datos)
        self.nombre_mol.set_text(archivo)
        self.b_cancelar.connect("clicked", self.cerrar)
        self.b_guardar.connect("clicked", self.guardar)

    def cerrar(self, btn=None):
        self.ventana.close()

    def guardar(self, btn=None):
        with open(self.archivo, 'w') as molecula:
            molecula.writee(self.info_mol.get_text())

if __name__ == "__main__":
    ventana = visualizar()
    Gtk.main()
